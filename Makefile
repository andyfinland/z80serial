# Makefile

all: clean z80serial

clean:
		-@rm -f z80serial
		-@rm -f z80dump
		-@rm -f z80run

z80serial:
		gcc -o z80serial z80serial.c
		gcc -o z80dump z80dump.c
		gcc -o z80run z80run.c
