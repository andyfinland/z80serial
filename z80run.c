/*
* Compile with:
* gcc -o z80serial z80serial.h z80serial.c
* or use the Makefile
*/

#include "z80serial.h"

// main
int main(void) {

  // open port 
  int port = openPort("/dev/tty.usbserial-A50285BI");

  if (port>0) {

    // config port 115200
    configPort(port);
    
    // send command
    writePort(port, "run");

    // print output
    char * str = readPort(port);
    printf("%s",str);

    // close port
    closePort(port);
    
    exit(EXIT_SUCCESS);

  } else {

    exit(EXIT_FAILURE);

  }

}



