/*
* Compile with:
* gcc -o z80serial z80serial.h z80serial.c
* or use the Makefile
*/

#include "z80serial.h"

// main
int main(int argc, char** argv) {

  if (argc < 3) {
    printf("Syntax: z80serial <device> <command>\n");
    exit(EXIT_FAILURE);
  }

  // open port 
  int port = openPort(argv[1]);

  if (port>0) {

    // config port 115200
    configPort(port);
    
    // send command
    writePort(port, argv[2]);

    // print output
    char * str = readPort(port);
    printf("%s",str);

    // close port
    closePort(port);
    
    exit(EXIT_SUCCESS);

  } else {

    exit(EXIT_FAILURE);

  }

}


