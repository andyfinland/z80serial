/*
* z80Serial
*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <stdlib.h>

// functions
int openPort(char * device);
int configPort(int fd);
int writePort(int fd, char * command);
int flushPort(int fd);
char * readPort(int fd);
int closePort(int fd);
int getByteCount();
char* concat(const char *s1, const char *s2);

// global variables
int lastBytes=0;



// open serial port
int openPort(char * device) {
  
  int fd = open(device, (O_RDWR | O_NOCTTY | O_NDELAY));

  if (fd == -1)
  {
      perror("Unable to open port");
      exit(EXIT_FAILURE);
  } else {
      fcntl(fd, F_SETFL, 0);
  }

  return (fd);
}

// write to serial port
int writePort(int fd, char * str) {
  char * combo = concat(str,"\r\n");
  int bytes = write(fd, combo, sizeof(combo)+2);
  return bytes;
}

// read serial port
char * readPort(int fd) {

  char result[65535];   // 64K
  memset(&result, '\0', sizeof(result));

  char buf[512];
  memset(&buf, '\0', sizeof(buf));
  
  int bytes = 0;
  int i = 0;
  char * ptr;
  int eot = 0x04;
  int stop=1;
  int r=0;

  while (stop==1) {

    bytes = read(fd, &buf, sizeof(buf));

    for(i=0;i<bytes;i++)	{

      if (buf[i]==0x10 || buf[i]==eot) {
        printf("HEX: %#04x\r\n",buf[i]);
        stop=0;
        break;
      } else {
        result[r] = buf[i];
        r++;
      }
      
    }
  }

  lastBytes = r;
  char * str = result;
  return str;

}

// close serial port
int closePort(int fd) {
    return close(fd);
}



// flush serial
int flushPort(int fd)
{
    sleep(2); //required to make flush work, for some reason
    return tcflush(fd, TCIOFLUSH);
}

// Set serial port attributes
int configPort(int fd) {

  struct termios options;
  tcgetattr(fd, &options);

  cfsetispeed(&options, B115200);
  cfsetospeed(&options, B115200);

  options.c_iflag &= ~(INLCR | ICRNL);
  options.c_iflag |= IGNPAR | IGNBRK;
  options.c_oflag &= ~(OPOST | ONLCR | OCRNL);
  options.c_cflag &= ~(PARENB | PARODD | CSTOPB | CSIZE | CRTSCTS);
  options.c_cflag |= CLOCAL | CREAD | CS8;
  options.c_lflag &= ~(ICANON | ISIG | ECHO);
  options.c_cc[VTIME] = 1;
  options.c_cc[VMIN]  = 0;

  if (tcsetattr(fd, TCSANOW, &options) < 0) {
      printf("Error setting serial port attributes\n");
      close(fd);
      return 0;
  } else {
      return 1;
  }
}

// get last bytes received
int getByteCount() {
    return lastBytes;
}

char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}
